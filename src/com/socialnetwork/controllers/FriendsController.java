package com.socialnetwork.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.socialnetwork.entities.NewsMessagePrototype;
import com.socialnetwork.entities.User;
import com.socialnetwork.services.MessageService;
import com.socialnetwork.services.UserService;

@Controller
public class FriendsController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private MessageService messageService; 

    @RequestMapping(value = "/friends.action", method = RequestMethod.GET)
    public ModelAndView getFriends(@CookieValue(value = "hash") String hash) {

        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        List<User> friendsList = userService.getFriendsList(user.getId());
        ModelAndView mav = new ModelAndView("friends");
        mav.addObject("user", user);
        mav.addObject("friendsList", friendsList);

        return mav;
    }

    @RequestMapping(value = "/addtofriends.action", method = RequestMethod.POST)
    public ModelAndView postAddToFriends(
            @RequestParam(value = "friendid", required = true) int friendId,
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        int userId = user.getId();

        userService.addToFriends(userId, friendId);
              
        return new ModelAndView("redirect:profile.action?id=" + friendId);
    }

    @RequestMapping(value = "/deletefromfriends.action", method = RequestMethod.GET)
    public ModelAndView getDeleteFromFriends(
            @RequestParam(value = "friendid", required = true) int friendId,
            @CookieValue(value = "hash", required = true) String hash) {

        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        int userId = user.getId();

        userService.deleteFromFriends(userId, friendId);
        
        
        NewsMessagePrototype newsMessagePrototype = new NewsMessagePrototype();
        newsMessagePrototype.setSender(user);        
        newsMessagePrototype.setMessageType(4);
        newsMessagePrototype.setObjectId(friendId);
        messageService.addNews(newsMessagePrototype);
        
        
        NewsMessagePrototype newsMessagePrototype2 = new NewsMessagePrototype();
        newsMessagePrototype2.setSender(userService.getUser(friendId));        
        newsMessagePrototype2.setMessageType(4);
        newsMessagePrototype2.setObjectId(user.getId());
        messageService.addNews(newsMessagePrototype2);
        
        return new ModelAndView("redirect:friends.action");
    }

    @RequestMapping(value = "/addtoignore.action", method = RequestMethod.POST)
    public ModelAndView postAddToIgnore(
            @RequestParam(value = "ignoreUserId", required = true) int ignoreUserId,
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        userService.addToIgnore(user.getId(), ignoreUserId);

        return new ModelAndView("redirect:profile.action?id=" + ignoreUserId);
    }
    
    @RequestMapping(value = "/addfriendtoignore.action", method = RequestMethod.POST)
    public ModelAndView addFriendToIgnore(
            @RequestParam(value = "ignoreUserId", required = true) int ignoreUserId,
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        userService.addFriendToIgnore(user.getId(), ignoreUserId);

        return new ModelAndView("redirect:friends.action");
    }


    @RequestMapping(value = "/ignore.action", method = RequestMethod.GET)
    public ModelAndView getIgnore(
            @CookieValue(value = "hash", required = true) String hash) {
        ModelAndView mav = new ModelAndView("ignoreusers");
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        mav.addObject("user", user);
        
        List<User> ignoreList = userService.getIgnoreList(user.getId());
        mav.addObject("ignoreList", ignoreList);

        return mav;
    }

    @RequestMapping(value = "/deletefromignore.action", method = RequestMethod.GET)
    public ModelAndView getDeleteFromIgnore(
            @RequestParam(value = "userignoreid", required = true) int userIgnoreId,
            @CookieValue(value = "hash", required = true) String hash) {

        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        int userId = user.getId();
        userService.deleteFromIgnore(userId, userIgnoreId);

        return new ModelAndView("redirect:ignore.action");
    }
    
    @RequestMapping(value = "/incomefrienship.action", method = RequestMethod.GET)
    public ModelAndView getIncomeFrienship(
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        List<User> requestFriendshipList = userService.getRequestFriendshipList(user.getId());
        ModelAndView mav = new ModelAndView("incomrequestfriends");
        mav.addObject("user", user);
        mav.addObject("requestFriendshipList", requestFriendshipList);
        return mav;
    }
    
    @RequestMapping(value = "/sendfrienship.action", method = RequestMethod.GET)
    public ModelAndView getSendFrienship(
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        List<User> sendRequestFriendshipList = userService.getSendRequestFriendshipList(user.getId());
        ModelAndView mav = new ModelAndView("sendrequestfriends");
        mav.addObject("user", user);
        mav.addObject("sendRequestFriendshipList", sendRequestFriendshipList);
        return mav;
    }
    
    @RequestMapping(value = "/acceptfriendship.action", method = RequestMethod.GET)
    public ModelAndView getAcceptFriendship(
            @RequestParam(value = "friendid", required = true) int friendId,
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        int userId = user.getId();

        userService.acceptFriendship(userId, friendId);
        
        NewsMessagePrototype newsMessagePrototype = new NewsMessagePrototype();
        newsMessagePrototype.setSender(user);        
        newsMessagePrototype.setMessageType(1);
        newsMessagePrototype.setObjectId(friendId);
        messageService.addNews(newsMessagePrototype);
        
        NewsMessagePrototype newsMessagePrototype2 = new NewsMessagePrototype();
        newsMessagePrototype2.setSender(userService.getUser(friendId));        
        newsMessagePrototype2.setMessageType(1);
        newsMessagePrototype2.setObjectId(user.getId());
        messageService.addNews(newsMessagePrototype2);
        
        return new ModelAndView("redirect:incomefrienship.action");
    }
    
    @RequestMapping(value = "/declinefriendship.action", method = RequestMethod.GET)
    public ModelAndView getDeclineFriendships(
            @RequestParam(value = "friendid", required = true) int friendId,
            @CookieValue(value = "hash", required = true) String hash) {
        User user = userService.getUserByHash(hash);
        if (user == null) {
            return new ModelAndView("redirect:./");
        }
        userService.declineFriendship(user.getId(), friendId);
        
        return new ModelAndView("redirect:incomefrienship.action");
    }


}