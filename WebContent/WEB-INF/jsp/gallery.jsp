<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Моя Галерея</title>
		<link href="webjars/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
		<link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
		<link href="http://getbootstrap.com/examples/navbar/navbar.css" rel="stylesheet">
		<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
		<script src="webjars/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<jsp:include page="menu.jsp" /><br>
			<h3>Альбоми користувача ${galleryOwner.getFirstName()} ${galleryOwner.getSecondName()}</h3>
			
			<c:choose>
				<c:when test="${albumsList.isEmpty()}">
					<p>У вас немає альбомів</p>
				</c:when>
				<c:otherwise>
					<c:forEach var="each" items="${albumsList}">	
						<table style="margin: 5px">
							<tr>
								<td>
									<div>
										<a href="album.action?id=${each.getId()}" class="btn btn-default">
											<strong>${each.getTitle()}</strong> 
											<br>
											Альбом створено: ${each.getFormatedDate()}
											<br> 
											Фотографій в альбомі: ${each.getPhotosCount()}
										</a>
									</div>
								</td>
								<c:if test="${galleryOwner.getId() == user.getId()}">
										<td style="padding-bottom: 60px">
											<form method="post" action="deletealbum.action">
												<input type="hidden" name="deletepath" value="galleries/${each.getOwnerId()}/${each.getTitle()}">
												<input type="hidden" name="albumid" value="${each.getId()}">
												<button class="close">&times;</button>							
											</form>
										</td>
										<td>
											<form method="post" action="renamealbum.action">
												<input type="hidden" name="folderpath" value="galleries/${each.getOwnerId()}/${each.getTitle()}">
												<input type="hidden" name="albumid" value="${each.getId()}">
												<input type="text" name="foldername" placeholder="Нове ім'я" required><br>
												<button class="btn btn-default" type="submit" style="margin-top: 5px">Змінити назву</button>							
											</form>
										<td>
								</c:if>
							</tr>
						</table>	 
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<c:if test="${user.getId() == galleryOwner.getId()}">
				<br>
				<br>
				<div class="input-group">
					<form method="post" action="gallery.action">
						<input type="text" name="albumname" class="form-control" placeholder="Введіть назву..." required>
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">Створити новий альбом</button>
						</span>
					</form>
				</div>
			</c:if>
	
		</div>
	</body>
</html>