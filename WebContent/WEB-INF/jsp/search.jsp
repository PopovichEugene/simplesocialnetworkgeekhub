<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Соціальна мережа - ${thisUser.getFirstName()} ${thisUser.getSecondName()}</title>
		<link href="webjars/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
		<link href="webjars/bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet">
		<link href="http://getbootstrap.com/examples/navbar/navbar.css" rel="stylesheet">
		<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
		<script src="webjars/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<title>Пошук користувачів</title>
	</head>
	<body>
		<div class="container">
			<jsp:include page="menu.jsp" /><br>
			<h1>Пошук користувачів</h1>
			<div style="width: 400px; float: left">
				<form method="post" action="search.action">
					<input type="text" name="username" placeholder="Ім'я">
					<br>
					<br>
					<input type="text" name="secondname" placeholder="Прізвище">
					<br>
					<br>
					<select name="gender">
						<option value="">Виберіть стать</option>
						<option value="1">Чоловіча</option>
						<option value="2">Жіноча</option>
					</select>
					<br>
					<br>
					<input type="text" name="fromage" placeholder="вік від" pattern="\d{2}">
					<input type="text" name="toage" placeholder="вік до" pattern="\d{2}">
					<br>
					<br>
					<select name="country">
						<option value="">Виберіть країну</option>
						<c:forEach var="each" items="${countriesList}">
							<option value="${each.getId()}">${each.getName()}</option>
						</c:forEach>
					</select>
					<br>
					<br>
					<input type="text" name="city" placeholder="Місто">
					<br>
					<br>
					<input type="text" name="phonenumber" placeholder="+380979132103">
					<br>
					<br>
					<input type="text" name="email" placeholder="e-mail">
					<br>
					<br>
					<input type="text" name="school" placeholder="Школа № 11">
					<br>
					<br>
					<input type="text" name="university" placeholder="ЧДТУ">
					<br>
					<br>
					<input type="submit" type="button" class="btn btn-default" value="Пошук">
					<input type="reset" type="button" class="btn btn-default" value="Скинути">
				</form>
			</div>
							
			<c:choose>
				<c:when test="${usersList.isEmpty()}">
					<p>Користувачів по таким даним не знайдено</p>
				</c:when>
				<c:otherwise>
					<c:forEach var="eachUser" items="${usersList}">	
						<div style="width: 210px; float: left;">
							<a href="profile.action?id=${eachUser.getId()}">
								<img width="100" height="100" src="${eachUser.getAvatarPath()}"><br>
								${eachUser.getFirstName()} ${eachUser.getSecondName()}
							</a>
						</div>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
		
	</body>
</html>